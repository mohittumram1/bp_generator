# HTML/CSS/JS Boilerplate Extension for Visual Studio Code

This Visual Studio Code (VSCode) extension allows you to generate HTML, CSS, and JavaScript files with boilerplate code directly in your workspace.

## Features

- Quickly create HTML, CSS, and JavaScript files with boilerplate code.
- Generate all three files with a single command.
- Optionally name your project and create a folder for the files.

## Getting Started

### Prerequisites

- Visual Studio Code (VSCode) editor.
- Node.js (npm) installed on your machine.

### Installation

1. Open the VSCode editor.
2. Go to the Extensions view (Ctrl+Shift+X) and search for "HTML/CSS/JS Boilerplate".
3. Click on "Install" to install the extension.

### Usage

1. Open a folder (workspace) in VSCode where you want to generate the files.
2. Press `Ctrl+Shift+P` (Cmd+Shift+P on macOS) to open the command palette.
3. Type "Generate Project with HTML, CSS, and JavaScript Boilerplate" and select the option.
4. Enter a project name when prompted (optional). This will create a folder with the project name and place the files inside it.
5. The extension will generate `index.html`, `styles.css`, and `script.js` files with boilerplate code.

## Customization

You can customize the boilerplate code in the `extension.ts` file. Look for the functions `getHtmlBoilerplate`, `getCssBoilerplate`, and `getJsBoilerplate` to modify the content as per your requirements.

## Acknowledgments

- This extension was inspired by the need to quickly generate boilerplate code for HTML, CSS, and JavaScript files in VSCode.

---

Happy coding! 🚀
